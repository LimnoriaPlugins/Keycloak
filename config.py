###
# Copyright (c) 2021, Georg Pfuetzenreuter
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

from supybot import conf, registry
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('Keycloak')
except:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


def configure(advanced):
    # This will be called by supybot to configure this module.  advanced is
    # a bool that specifies whether the user identified themself as an advanced
    # user or not.  You should effect your configuration by manipulating the
    # registry as appropriate.
    from supybot.questions import expect, anything, something, yn
    conf.registerPlugin('Keycloak', True)


Keycloak = conf.registerPlugin('Keycloak')
# This is where your configuration variables (if any) should go.  For example:
# conf.registerGlobalValue(Keycloak, 'someConfigVariableName',
#     registry.Boolean(False, _("""Help for someConfigVariableName.""")))

###
# API backend related settings below:
###
conf.registerGroup(Keycloak, 'backend')
conf.registerGlobalValue(Keycloak.backend, 'server',
    registry.String('https://example.com',
    """
    Keycloak: Instance hostname in the format https://example.com - the rest of the URL will be constructed by the system.
    """
    , private=True
))
conf.registerGlobalValue(Keycloak.backend, 'realm',
    registry.String('MyRealm',
    """
    Keycloak: Realm name with exact capitalization.
    """
    , private=True
))
conf.registerGlobalValue(Keycloak.backend, 'token',
    registry.String('InsanelyLongTokenConsistingOfRandomChars',
    """
    Keycloak: Access/Bearer/OIDC token. NOT a client secret. NOT a token with MASTER realm access. NOT a token with excess access roles.
    """
    , private=True
))

###
# User replies settings below:
###
conf.registerGroup(Keycloak, 'replies')
conf.registerGlobalValue(Keycloak.replies, 'error',
    registry.String('Something went wrong. Please contact an administrator.',
    """
    Keycloak: Response to show the user if any kind of error occured. Note that exact error details will alwasy be printed exclusively to the console.
    """
    , private=False
))

###
# API call settings below:
###
conf.registerGroup(Keycloak, 'options')
conf.registerGlobalValue(Keycloak.options, 'emailVerified',
    registry.Boolean(False,
    """
    Keycloak: Whether to set newly created users email addresses to having been verified \(true, default\) or not \(false\)
    """
    , private=True
))
conf.registerGlobalValue(Keycloak.options, 'firstName',
    registry.String('Foo',
    """
    Keycloak: What to set as the firstName value for newly created users.
    """
    , private=True
))
conf.registerGlobalValue(Keycloak.options, 'lastName',
    registry.String('Bar',
    """
    Keycloak: What to set as the lastName value for newly created users.
    """
    , private=True
))
conf.registerGlobalValue(Keycloak.options, 'ircgroup',
    registry.String('',
    """
    Keycloak: Group ID for `ircprom`
    """
    , private=True
))
conf.registerGlobalValue(Keycloak.options, 'confluencegroup',
    registry.String('',
    """
    Keycloak: Group ID for admin grant: confluencegroup
    """,
    private=True
))

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
